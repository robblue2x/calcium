import React from 'react';
import { Grid } from 'semantic-ui-react';

export default class DayNames extends React.Component {
  render() {
    const dayNames = [];
    for (let i = 0; i < 7; i++) {
      const d = new Date(2000, 1, i + 6);
      const a = d.toLocaleDateString(undefined, { weekday: 'short' });
      dayNames.push(<Grid.Column key={a}>{a}</Grid.Column>);
    }
    return <Grid.Row>{dayNames}</Grid.Row>;
  }
}
