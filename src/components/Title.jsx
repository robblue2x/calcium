import React from 'react';
import { Header, Segment } from 'semantic-ui-react';

export default class Title extends React.Component {
  render() {
    const { title } = this.props;
    return (
      <Segment basic textAlign="center">
        <Header as="h1">{title}</Header>
      </Segment>
    );
  }
}
