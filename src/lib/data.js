/* eslint no-param-reassign:off */

const { EventEmitter } = require('events');
const { genYear } = require('./generate');
const { toDayNo } = require('./convert');
const selected = require('./selected');

const emitter = new EventEmitter();
const list = genYear();

const setSelections = l => {
  list.forEach(m => m.forEach(d => (d.selected = l.includes(toDayNo(d)))));
  emitter.emit('update');
};

selected.on('update', setSelections);
setSelections(selected.get());

const get = () => list;
const on = emitter.on.bind(emitter);

module.exports = { get, on };
