/* eslint no-return-assign: off */

const { add, remove } = require('./selected');

let mouseDown = false;
let selecting = true;

const mouseDowned = d => {
  mouseDown = true;
  selecting = !d.selected;
  if (!d.selected) {
    add(d);
  } else {
    remove(d);
  }
};

const mouseMoved = d => {
  if (mouseDown) {
    if (selecting) {
      add(d);
    } else {
      remove(d);
    }
  }
};

window.addEventListener('mouseup', () => (mouseDown = false));

module.exports = { mouseDowned, mouseMoved };
