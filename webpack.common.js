const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const { title } = JSON.parse(fs.readFileSync('./package.json'));
assert(title, 'No title specified in package.json');

const { DEMO_MODE, BASE_PATH } = process.env;

if (DEMO_MODE) {
  console.log('DEMO_MODE --- DEMO_MODE --- DEMO_MODE');
}

const config = {
  entry: {
    index: './src/index.jsx'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[hash].js',
    sourceMapFilename: '[name].[hash].map'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  plugins: [
    new webpack.DefinePlugin({ DEMO_MODE, BASE_PATH: JSON.stringify(BASE_PATH) }),

    new CleanWebpackPlugin(['dist']),

    new CopyWebpackPlugin([
      { from: 'assets', to: 'assets' },
      { from: 'node_modules/semantic-ui-css/semantic.min.css', to: 'assets/' },
      { from: 'node_modules/semantic-ui-css/themes', to: 'assets/themes' },
      { from: path.join(__dirname, 'README.md'), to: 'assets/' }
    ]),

    new HtmlWebpackPlugin({
      title,
      template: path.join(__dirname, 'src', 'index.ejs')
    })
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/preset-react', '@babel/preset-env']
        }
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx']
  }
};

if (!DEMO_MODE) {
  config.plugins.push(new webpack.IgnorePlugin(/.\/fake/));
}

module.exports = config;
